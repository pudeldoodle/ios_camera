//
//  ViewController.h
//  camera
//
//  Created by Pudel_dev on 22/06/15.
//  Copyright (c) 2015 Pudel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController<UIImagePickerControllerDelegate,UINavigationControllerDelegate>

@property (strong, nonatomic) IBOutlet UIImageView *imageView;
@property (strong, nonatomic) IBOutlet UIButton *takeButton;
@property (strong, nonatomic) IBOutlet UIButton *selectButton;
- (IBAction)takeAction:(id)sender;
- (IBAction)selectAction:(id)sender;


@end

